//
//  AdjustFrameNavigationController.h
//  smiths0n
//
//  Created by Alex Smithson on 23/05/2013.
//  Copyright (c) 2013 smiths0n. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdjustFrameNavigationController : UINavigationController

@end
