//
//  UINavigationItem+Extensions.m
//  smiths0n
//
//  Created by Alex Smithson on 26/02/2013.
//  Copyright (c) 2013 hedgehog lab. All rights reserved.
//

#import "UINavigationItem+Extensions.h"

@implementation UINavigationItem (Extensions)

//Ready for changing to custom styling for title
-(void)changeTitle:(NSString*)title;
{
    [self setTitle:title
         withStyle:@"navigation-bar-title"];
}

-(void)setTitle:(NSString *)title withStyle:(NSString*)style;
{
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [titleLabel setText:title];
    [titleLabel setStyleClass:style];
    [titleLabel setShadowColor:[UIColor blackColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [self setTitleView:titleLabel];
    [titleLabel sizeToFit];
    RELEASE(titleLabel);
}

@end
