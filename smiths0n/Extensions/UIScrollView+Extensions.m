//
//  UIScrollView+Extensions.m
//  smiths0n
//
//  Created by Alex Smithson on 30/04/2013.
//  Copyright (c) 2013 smiths0n. All rights reserved.
//

#import "UIScrollView+Extensions.h"

@implementation UIScrollView (Extensions)

-(void)layoutScrollView;
{
    [self layoutScrollViewWithPadding:CGSizeZero];
}

-(void)layoutScrollViewWithPadding:(CGSize)padding;
{
    [self setContentSize:[self maxYViewWithPadding:padding]];
}

-(float)contentHeight;
{
    return self.contentSize.height;
}

@end
