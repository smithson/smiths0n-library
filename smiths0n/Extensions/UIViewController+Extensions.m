//
//  UIViewController+Extensions.m
//  smiths0n
//
//  Created by Alex Smithson on 27/08/2012.
//  Copyright  2012 Alex Smithson. All rights reserved.
//

#import "UIViewController+Extensions.h"

@implementation UIViewController (Extensions)


//The correct frame function for sorting ipad
+(CGRect)frameWithNav;
{
    CGRect screenSize = [UIScreen mainScreen].bounds;
    if (IS_IPAD) {
        screenSize = CGRectMake(screenSize.origin.y, screenSize.origin.x, screenSize.size.height, screenSize.size.width);
        return CGRectMake(0, 0, screenSize.size.width, screenSize.size.height);
    }
    
    //Status bar
    screenSize.size.height-=20;
    
    return screenSize;
}

//Frame size without the navigation bar
+(CGRect)correctFrame;
{
    CGRect rect = [self frameWithNav];
    rect.size.height-=44;
    return rect;
}

+(CGRect)sideMenuFrame;
{
    CGRect rect = [self frameWithNav];
    rect.size.width-=IS_IPAD?(rect.size.width-(320-54)):54;
    return rect;
}

//Method to fix size
-(void)adjustFrame;
{
    [self.view setFrame:[UIViewController correctFrame]];
}

-(void)displayNotification:(NSString*)text;
{
    UIFont *font = [UIFont fontWithName:@"Comfortaa" size:16.0f];
    CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(CGRectGetWidth(self.view.bounds)-40, MAXFLOAT)];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, -(size.height+10), CGRectGetWidth(self.view.bounds), size.height+10)];
    [view setStyleClass:@"notification-header"];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, CGRectGetWidth(view.frame)-40, size.height)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setNumberOfLines:0];
    [label setFont:font];
    [label setText:text];
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [view addSubview:label];
    
    [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleNotification:)]];
    
    [self.view addSubview:view];
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = view.frame;
        frame.origin.y=0;
        [view setFrame:frame];
    } completion:^(BOOL finished) {
        [self performSelector:@selector(removeNotification:) withObject:view afterDelay:2.0];
    }];
}

-(void)handleNotification:(UITapGestureRecognizer*)gesture;
{
    if ([gesture state]==UIGestureRecognizerStateRecognized) {
        [self removeNotification:[gesture view]];
    }
}

-(void)removeNotification:(UIView*)notification;
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(removeNotification:) object:notification];
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = notification.frame;
        frame.origin.y-=frame.size.height;
        [notification setFrame:frame];
    } completion:^(BOOL finished) {
        [notification removeFromSuperview];
    }];
}

+(CGRect)modalFrame;
{
    CGRect rect = [self correctFrame];
    if (IS_IPAD) {
        rect = CGRectMake(0, 0, 540, 620);
    }
    
    return rect;
}

-(void)adjustModalFrame;
{
    [self.view setFrame:[UIViewController modalFrame]];
}

-(BOOL)isModal {
    
    BOOL isModal = ((self.parentViewController && self.parentViewController.modalViewController == self) ||
                    //or if I have a navigation controller, check if its parent modal view controller is self navigation controller
                    ( self.navigationController && self.navigationController.parentViewController && self.navigationController.parentViewController.modalViewController == self.navigationController) ||
                    //or if the parent of my UITabBarController is also a UITabBarController class, then there is no way to do that, except by using a modal presentation
                    [[[self tabBarController] parentViewController] isKindOfClass:[UITabBarController class]]);
    
    //iOS 5+
    if (!isModal && [self respondsToSelector:@selector(presentingViewController)]) {
        
        isModal = ((self.presentingViewController && self.presentingViewController.modalViewController == self) ||
                   //or if I have a navigation controller, check if its parent modal view controller is self navigation controller
                   (self.navigationController && self.navigationController.presentingViewController && self.navigationController.presentingViewController.modalViewController == self.navigationController) ||
                   //or if the parent of my UITabBarController is also a UITabBarController class, then there is no way to do that, except by using a modal presentation
                   [[[self tabBarController] presentingViewController] isKindOfClass:[UITabBarController class]]);
        
    }
    
    return isModal;
    
}

@end