//
//  UIScrollView+Extensions.h
//  smiths0n
//
//  Created by Alex Smithson on 30/04/2013.
//  Copyright (c) 2013 smiths0n. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Extensions)

-(void)layoutScrollView;
-(void)layoutScrollViewWithPadding:(CGSize)padding;

-(float)contentHeight;

@end
