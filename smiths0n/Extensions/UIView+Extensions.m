//
//  UIView+Extensions.m
//  smiths0n
//
//  Created by Alex Smithson on 21/03/2013.
//  Copyright (c) 2013 hedgehog lab. All rights reserved.
//

#import "UIView+Extensions.h"

@implementation UIView (Extensions)

-(void)layoutView;
{
    [self layoutViewWithPadding:CGSizeZero];
}

-(void)layoutViewWithPadding:(CGSize)padding;
{
    CGSize size = [self maxYViewWithPadding:padding];
    
    CGRect frame = self.frame;
    frame.size = size;
    [self setFrame:frame];
}

-(CGSize)maxYView;
{
    return [self maxYViewWithPadding:CGSizeZero];
}

-(CGSize)maxYViewWithPadding:(CGSize)padding;
{
    CGSize size = self.frame.size;
    size.height=0;
    
    for (UIView *view in self.subviews) {
        CGSize viewSize = CGSizeMake(CGRectGetMaxX(view.frame), CGRectGetMaxY(view.frame));
        
        //        if (viewSize.width>size.width) {
        //            size.width=viewSize.width;
        //        }
        
        if (viewSize.height>size.height) {
            size.height=viewSize.height;
        }
    }
    
    //Add padding
    size.width+=padding.width;
    size.height+=padding.height;
    
    return size;
}

-(void)setBorderColor:(UIColor*)color withBorderWidth:(float)width;
{
    [self setBorderColor:color withBorderWidth:width withCornerRadius:0];
}

-(void)setBorderColor:(UIColor*)color withBorderWidth:(float)width withCornerRadius:(float)radius;
{
    [self setBorderColor:color withBorderWidth:width withCornerRadius:radius includeShadow:NO];
}

-(void)setBorderColor:(UIColor*)color withBorderWidth:(float)width withCornerRadius:(float)radius includeShadow:(BOOL)shadow;
{
    [[self layer] setBorderColor:color.CGColor];
    [[self layer] setBorderWidth:width];
    [[self layer] setCornerRadius:radius];
    [[self layer] setMasksToBounds:YES];
    
    if (shadow)
        [self applyShadowWithCornerRadius:radius];
}

-(void)applyShadow;
{
    [self applyShadowWithCornerRadius:0];
}

-(void)applyShadowWithCornerRadius:(float)cornerRadius;
{
    [[self layer] setShadowColor:[UIColor blackColor].CGColor];
    [[self layer] setShadowOffset:CGSizeZero];
    [[self layer] setShadowOpacity:0.1];
    [[self layer] setShadowPath:[UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:cornerRadius].CGPath];
    [[self layer] setMasksToBounds:YES];
}

@end
