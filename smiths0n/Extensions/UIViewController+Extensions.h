//
//  UIViewController+Extensions.h
//  smiths0n
//
//  Created by Alex Smithson on 27/08/2012.
//  Copyright  2012 Alex Smithson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Extensions)

+(CGRect)frameWithNav;
+(CGRect)correctFrame;

+(CGRect)sideMenuFrame;

-(void)adjustFrame;

+(CGRect)modalFrame;
-(void)adjustModalFrame;

-(void)displayNotification:(NSString*)text;

-(BOOL)isModal;

@end