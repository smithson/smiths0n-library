//
//  NSMutableArray+Extensions.m
//  smiths0n
//
//  Created by Alex Smithson on 27/02/2013.
//  Copyright (c) 2013 hedgehog lab. All rights reserved.
//

#import "NSMutableArray+Extensions.h"

@implementation NSMutableArray (Extensions)

-(void)addInt:(int)intValue;
{
    [self addObject:[NSNumber numberWithInt:intValue]];
}

-(void)removeInt:(int)intValue;
{
    [self removeObject:[NSNumber numberWithInt:intValue]];
}

-(NSUInteger)indexOfInt:(int)intValue;
{
    return [self indexOfObject:[NSNumber numberWithInt:intValue]];
}

-(BOOL)containsInt:(int)intValue;
{
    return [self containsObject:[NSNumber numberWithInt:intValue]];
}

@end
