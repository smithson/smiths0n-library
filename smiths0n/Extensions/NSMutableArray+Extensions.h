//
//  NSMutableArray+Extensions.h
//  smiths0n
//
//  Created by Alex Smithson on 27/02/2013.
//  Copyright (c) 2013 hedgehog lab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Extensions)

-(void)addInt:(int)intValue;
-(void)removeInt:(int)intValue;
-(NSUInteger)indexOfInt:(int)intValue;
-(BOOL)containsInt:(int)intValue;

@end
