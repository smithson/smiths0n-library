//
//  UINavigationItem+Extensions.h
//  smiths0n
//
//  Created by Alex Smithson on 26/02/2013.
//  Copyright (c) 2013 hedgehog lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationItem (Extensions)

-(void)changeTitle:(NSString*)title;

@end
