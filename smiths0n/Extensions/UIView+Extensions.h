//
//  UIView+Extensions.h
//  smiths0n
//
//  Created by Alex Smithson on 21/03/2013.
//  Copyright (c) 2013 hedgehog lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extensions)

-(CGSize)maxYView;
-(CGSize)maxYViewWithPadding:(CGSize)padding;

-(void)layoutView;
-(void)layoutViewWithPadding:(CGSize)padding;

-(void)setBorderColor:(UIColor*)color withBorderWidth:(float)width;
-(void)setBorderColor:(UIColor*)color withBorderWidth:(float)width withCornerRadius:(float)radius;
-(void)setBorderColor:(UIColor*)color withBorderWidth:(float)width withCornerRadius:(float)radius includeShadow:(BOOL)shadow;

-(void)applyShadow;
-(void)applyShadowWithCornerRadius:(float)cornerRadius;

@end
