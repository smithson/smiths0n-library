//
//  AdjustFrameViewController.m
//  smiths0n
//
//  Created by Alex Smithson on 26/02/2013.
//  Copyright (c) 2013 hedgehog lab. All rights reserved.
//

#import "AdjustFrameViewController.h"

@interface AdjustFrameViewController ()

@end

@implementation AdjustFrameViewController


- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization        
    }
    return self;
}

//The correct frame function for sorting ipad
+(CGRect)frameWithNav;
{
    CGRect screenSize = [UIScreen mainScreen].bounds;
    if (IS_IPAD) {
        screenSize = CGRectMake(screenSize.origin.y, screenSize.origin.x, screenSize.size.height, screenSize.size.width);
        return CGRectMake(0, 0, screenSize.size.width, screenSize.size.height);
    }
    
    //Status bar
    screenSize.size.height-=20;
    
    return screenSize;
}

//Frame size without the navigation bar
+(CGRect)correctFrame;
{
    CGRect rect = [self frameWithNav];
    rect.size.height-=44;
    return rect;
}

+(CGRect)sideMenuFrame;
{
    CGRect rect = [self frameWithNav];
    rect.size.width-=IS_IPAD?(rect.size.width-(320-54)):54;
    return rect;
}

//Method to fix size
-(void)adjustFrame;
{
    [self.view setFrame:[AdjustFrameViewController correctFrame]];
}

-(void)loadView
{
    [super loadView];
    //Corrects the frame for ipad
    [self adjustFrame];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate;
{
    return YES;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation;
{
    if (IS_IPAD) return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

-(NSUInteger)supportedInterfaceOrientations;
{
    if (IS_IPAD) return UIInterfaceOrientationMaskLandscape;
    return UIInterfaceOrientationMaskPortrait;
}

@end
