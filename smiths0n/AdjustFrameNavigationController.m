//
//  AdjustFrameNavigationController.m
//  smiths0n
//
//  Created by Alex Smithson on 23/05/2013.
//  Copyright (c) 2013 smiths0n. All rights reserved.
//

#import "AdjustFrameNavigationController.h"

@interface AdjustFrameNavigationController ()

@end

@implementation AdjustFrameNavigationController

//The correct frame function for sorting ipad
+(CGRect)frameWithNav;
{
    CGRect screenSize = [UIScreen mainScreen].bounds;
    if (IS_IPAD) {
        screenSize = CGRectMake(screenSize.origin.y, screenSize.origin.x, screenSize.size.height, screenSize.size.width);
        return CGRectMake(0, 0, screenSize.size.width, screenSize.size.height);
    }
    
    //Status bar
    screenSize.size.height-=20;
    
    return screenSize;
}

//Frame size without the navigation bar
+(CGRect)correctFrame;
{
    CGRect rect = [self frameWithNav];
    rect.size.height-=44;
    return rect;
}

+(CGRect)sideMenuFrame;
{
    CGRect rect = [self frameWithNav];
    rect.size.width-=IS_IPAD?(rect.size.width-(320-54)):54;
    return rect;
}

//Method to fix size
-(void)adjustFrame;
{
    [self.view setFrame:[AdjustFrameViewController correctFrame]];
}

-(void)loadView
{
    [super loadView];
    //Corrects the frame for ipad
    [self adjustFrame];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate;
{
    return YES;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation;
{
    if (IS_IPAD) return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

-(NSUInteger)supportedInterfaceOrientations;
{
    if (IS_IPAD) return UIInterfaceOrientationMaskLandscape;
    return UIInterfaceOrientationMaskPortrait;
}

@end
