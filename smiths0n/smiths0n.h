//
//  smiths0n.h
//  smiths0n
//
//  Created by Alex Smithson on 21/05/2013.
//  Copyright (c) 2013 smiths0n. All rights reserved.
//

#import <Foundation/Foundation.h>

#define RELEASE(x) x=nil
#define APP_DELEGATE (AppDelegate*)[UIApplication sharedApplication].delegate
#define IS_IPAD ([[UIDevice currentDevice] respondsToSelector:@selector(userInterfaceIdiom)] && [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#define rgba(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define IS_RETINA ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] == YES && [[UIScreen mainScreen] scale] == 2.00)

//smiths0n Extensions
#import "NSMutableArray+Extensions.h"
#import "UINavigationItem+Extensions.h"
#import "UIScrollView+Extensions.h"
#import "UIView+Extensions.h"
#import "UIViewController+Extensions.h"
#import "AdjustFrameViewController.h"
#import "AdjustFrameNavigationController.h"

//UIImage Extensions
#import "UIImage+Resize.h"
#import "UIImage+Decode.h"
#import "UIImage+Alpha.h"
#import "UIImage+RoundedCorner.h"

//Other
#import "OrderedDictionary.h"
#import "NSDate+TimeAgo.h"

@interface smiths0n : NSObject

@end
