//
//  UIImage+JTImageDecode.h
//
//  Created by james on 9/28/11.
//  http://ioscodesnippet.tumblr.com
//

#import <UIKit/UIKit.h>

@interface UIImage (Decode)
- (UIImage *)pspdf_preloadedImage;
+ (UIImage *)pspdf_preloadedImageWithContentsOfFile:(NSString*)filepath;
+ (BOOL)isRetinaDisplay;
@end
