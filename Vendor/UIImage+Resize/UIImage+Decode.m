//
//  UIImage+JTImageDecode.m
//
//  Created by james on 9/28/11.
//  http://ioscodesnippet.tumblr.com
//

#import "UIImage+Decode.h"

@implementation UIImage (Decode)

- (UIImage *)pspdf_preloadedImage {
    CGImageRef image = self.CGImage;
    
    // make a bitmap context of a suitable size to draw to, forcing decode
    size_t width = CGImageGetWidth(image);
    size_t height = CGImageGetHeight(image);
    
    CGColorSpaceRef colourSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef imageContext =  CGBitmapContextCreate(NULL, width, height, 8, width*4, colourSpace,
                                                       kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little);
    CGColorSpaceRelease(colourSpace);
    
    // draw the image to the context, release it
    CGContextDrawImage(imageContext, CGRectMake(0, 0, width, height), image);
    
    // now get an image ref from the context
    CGImageRef outputImage = CGBitmapContextCreateImage(imageContext);
    
    UIImage *cachedImage = [UIImage imageWithCGImage:outputImage];
    
    // clean up
    CGImageRelease(outputImage);
    CGContextRelease(imageContext);
    
    return cachedImage;
}

+ (UIImage *)pspdf_preloadedImageWithContentsOfFile:(NSString*)imagePath {
    CGDataProviderRef dataProvider = CGDataProviderCreateWithFilename([imagePath UTF8String]);
    CGImageRef image = CGImageCreateWithJPEGDataProvider(dataProvider, NULL, NO, kCGRenderingIntentDefault);

    // make a bitmap context of a suitable size to draw to, forcing decode
    size_t width = CGImageGetWidth(image);
    size_t height = CGImageGetHeight(image);

    CGColorSpaceRef colourSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef imageContext =  CGBitmapContextCreate(NULL, width, height, 8, width*4, colourSpace,
                                                       kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little);
    
    CGColorSpaceRelease(colourSpace);

    // draw the image to the context, release it
    CGContextDrawImage(imageContext, CGRectMake(0, 0, width, height), image);

    // now get an image ref from the context
    CGImageRef outputImage = CGBitmapContextCreateImage(imageContext);

    UIImage* cachedImage = [UIImage imageWithCGImage:outputImage];

    // clean up
    CGImageRelease(outputImage);
    CGContextRelease(imageContext);
    CGImageRelease(image);
    CGDataProviderRelease(dataProvider);
    return cachedImage;
}

+ (BOOL)isRetinaDisplay {
	static dispatch_once_t predicate;
	static BOOL answer;
    
	dispatch_once(&predicate, ^{
		answer = ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2);
	});
	return answer;
}

@end 